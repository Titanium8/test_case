from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

link ="https://sbis.ru"
browser = webdriver.Chrome()
browser.maximize_window()
browser.get(link)

contacts = WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "li[class*=sbisru-Header__menu-item-1]"))).click()
image = WebDriverWait(browser, 10).until(EC.presence_of_element_located(( By.CSS_SELECTOR, "a[class*=sbisru-Contacts__logo-tensor]"))).click()

browser.switch_to.window(browser.window_handles[1])

WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "div[class*=tensor_ru-Index__block4-content]>p[class*=tensor_ru-Index__card-title]")))
title = browser.find_element(By.CSS_SELECTOR, "div[class*=tensor_ru-Index__block4-content]>p[class*=tensor_ru-Index__card-title]")

check = 'Сила в людях'
if title.text == check:
    print("Блок сила в людях существует.")
else:
    print(f"Ошибка: Ожидалось оглавление {check}, но текущее оглавление - '{title.text}'.")

about = WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "div[class*=tensor_ru-Index__block4-content]>p[class*=tensor_ru-Index__card-text]>a[class*=t]")))
browser.execute_script("arguments[0].click();", about)

current_url = browser.current_url
expected_url = "https://tensor.ru/about"

if current_url == expected_url:
    print("Успешный переход по ссылке 'about'.")
else:
    print(f"Ошибка: Ожидался URL '{expected_url}', но текущий URL - '{current_url}'.")

work_section = WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "div[class*=tensor_ru-About__block3]")))
work_section.click()

photos = WebDriverWait(browser, 10).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, "div[class*=tensor_ru-About__block3-image-filter]")))

first_photo = photos[0]
first_photo_width = first_photo.size['width']
first_photo_height = first_photo.size['height']

for photo in photos[1:]:
    photo_width = photo.size['width']
    photo_height = photo.size['height']

    if photo_width != first_photo_width or photo_height != first_photo_height:
        print("Ошибка: У фотографий разные размеры.")
        break
else:
    print("Все фотографии в разделе 'Работаем' имеют одинаковые размеры.")

browser.quit()