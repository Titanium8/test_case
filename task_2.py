from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

link = "https://sbis.ru"
browser = webdriver.Chrome()
browser.maximize_window()
browser.get(link)

contacts = WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "li[class*=sbisru-Header__menu-item-1]"))).click()

current_region = WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "span[class*=sbis_ru-link]")))
expected_region = 'Республика Башкортостан'

if current_region.text == expected_region:
    print("Наш регион определен корректно.")
else:
    print(f"Ошибка: Ожидался '{expected_region}', но текущий регион '{current_region}'.")

partnership_list = WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "div[class*=sbisru-Contacts-List__name]")))
expected_list = 'СБИС - Уфа'

if partnership_list.text == expected_list:
    print("Список партнеров определен корректно.")
else:
    print(f"Ошибка: Ожидался список партнеров '{expected_list}', но текущий список партнеров '{partnership_list}'.")

current_region.click()
link_region = WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "span[title*='Камчатский край']")))
link_region.click()

current_region = WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "span[class*=sbis_ru-link]")))
expected_region = 'Камчатский край'

if current_region.text == expected_region:
    print("Выбранный регион был изменен корректно.")
else:
    print(f"Ошибка: Ожидался '{expected_region}', но текущий регион '{current_region}'.")

partnership_list = WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "div[class*=sbisru-Contacts-List__name]")))
expected_list = 'СБИС - Камчатка'

if partnership_list.text == expected_list:
    print("Список партнеров успешно изменился.")
else:
    print(f"Ошибка: Ожидался список партнеров '{expected_list}', но текущий список партнеров '{partnership_list}'.")

current_url = browser.current_url
expected_url = "https://sbis.ru/contacts/41-kamchatskij-kraj?tab=clients"

if current_url == expected_url:
    print("URL успешно изменен.")
else:
    print(f"Ошибка: Ожидался URL '{expected_url}', но текущий URL - '{current_url}'.")

current_title = browser.title
expected_title = "СБИС Контакты — Камчатский край"

if expected_title.lower() in current_title.lower():
    print("Заголовок страницы успешно изменен.")
else:
    print(f"Ошибка: Ожидался заголовок '{expected_title}', но текущий заголовок - '{current_title}'.")

browser.quit()
